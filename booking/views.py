from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import redirect, render

from .forms import BookingForm, ClientForm


def index(request):
    return render(request, "main.html")


def about_clients(request):
    return render(request, "about/clients.html")


def about_history(request):
    return render(request, "about/history.html")


def menu(request):
    return render(request, "menu.html")


def booking(request):

    booking_form = BookingForm()
    client_form = ClientForm()
    if request.method == 'POST':
        booking_form = BookingForm(request.POST)
        client_form = ClientForm(request.POST)

        if booking_form.is_valid() and client_form.is_valid():
            booking = booking_form.save(commit=False)
            booking.client = client_form.save()
            booking.save()
            message = f'Бронирование на {booking.start_time} подтверждено.'
            send_mail(
                subject='Бронирование в ресторане Chili',
                message=message,
                from_email=settings.EMAIL_HOST_USER,
                # recipient_list=[booking.client.email]
                recipient_list=['konovalova.owl@gmail.com']
            )
            messages.success(request, message)
            return redirect('booking')
    return render(request, "booking.html", {
        'booking_form': booking_form,
        'client_form': client_form
    })


def contacts(request):
    return render(request, "contacts.html")
