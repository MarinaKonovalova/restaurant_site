from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('about/history/', views.about_history, name='about-history'),
    path('about/clients/', views.about_clients, name='about-clients'),
    path('menu/', views.menu, name='menu'),
    path('booking/', views.booking, name='booking'),
    path('contacts/', views.contacts, name='contacts'),
]
