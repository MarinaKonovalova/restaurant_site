from datetime import timedelta

from django import forms
from django.db.models import Q
from django.utils import timezone

from .models import Booking, Client, Table


class BookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = ('guests_count', 'start_time',)
        labels = {
            'guests_count': 'Количество персон',
            'start_time': 'Время',
        }
        widgets = {
            'guests_count': forms.TextInput(
                attrs={
                    'placeholder': labels['guests_count']
                }
            ),
            'start_time': forms.DateTimeInput(attrs={
                'data-target': '#datetimepicker1',
                'placeholder': labels['start_time']
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['start_time'].input_formats=['%d/%m/%Y %H:%M']

    def clean(self):
        cleaned_data = super().clean()
        if 'start_time' in cleaned_data and 'guests_count' in cleaned_data:
            if cleaned_data['start_time'] <= timezone.now() + timedelta(minutes=15):
                raise forms.ValidationError('Бронирование возможно не менее чем за 15 минут до прихода')
            start_time = cleaned_data['start_time']
            end_time = start_time + timedelta(hours=2)

            available_table = Table.objects.exclude(
                Q(booking__start_time__gte=start_time, booking__start_time__lt=end_time) |
                Q(booking__end_time__gte=end_time, booking__end_time__gt=start_time) |
                Q(booking__start_time__gte=start_time, booking__end_time__lte=end_time)
            ).filter(capacity__gte=cleaned_data['guests_count']).order_by('capacity').first()

            if available_table is None:
                raise forms.ValidationError(
                    'Нет свободных столиков, выберите другое время или измените количество гостей')
            self.cleaned_data['table'] = available_table
            self.cleaned_data['end_time'] = end_time

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.table = self.cleaned_data['table']
        instance.end_time = self.cleaned_data['end_time']
        if commit:
            instance.save()
        return instance


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ()
        labels = {
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'email': 'Email',
            'phone_number': 'Номер телефона',
        }
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': labels['first_name']
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': labels['last_name']
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': labels['email']
                }
            ),
            'phone_number': forms.TextInput(
                attrs={
                    'placeholder': labels['phone_number']
                }
            ),
        }
