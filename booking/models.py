from django.core.validators import RegexValidator, MinValueValidator
from django.db import models
from django.urls import reverse

phone_regex = RegexValidator(
    regex=r'^\+?1?\d{11}$',
    message="Формат '+79876543210' (11 цифр)"
)


class Client(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()

    phone_number = models.CharField(validators=[phone_regex], max_length=17)

    def get_absolute_url(self):
        return reverse('client', args=[str(self.id)])

    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)


class Table(models.Model):
    number = models.IntegerField(unique=True)
    capacity = models.IntegerField()

    def get_absolute_url(self):
        return reverse('table', args=[str(self.number)])

    def __str__(self):
        return str(self.number)


class Booking(models.Model):
    table = models.ForeignKey('Table', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    guests_count = models.IntegerField(verbose_name='Количество персон', validators=[MinValueValidator(1)])
    start_time = models.DateTimeField(verbose_name='Время начала')
    end_time = models.DateTimeField()

    def get_absolute_url(self):
        return reverse('booking', args=[str(self.id)])

    def __str__(self):
        return '%s, %s' % (self.table, self.start_time)
